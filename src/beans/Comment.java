package beans;

import java.io.Serializable;
import java.util.Date;

public class Comment implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private int message_id;
    private String text;
    private Date createdDate;
    private int user_id;


	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public int getId() {
		return id;
	}
	public int getMessage_id() {
		return message_id;
	}
	public String getText() {
		return text;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public int getUser_id() {
		return user_id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public void setMessage_id(int message_id) {
		this.message_id = message_id;
	}
	public void setText(String text) {
		this.text = text;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

}
