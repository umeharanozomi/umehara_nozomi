package beans;

import java.io.Serializable;

public class UserManagement implements Serializable {
    private static final long serialVersionUID = 1L;
    private int id;
    private String login_id;
    private String name;
    private int branch_id;
    private int position_id;
    private int is_deleted;
    private String branchName;
    private String positionName;
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public int getId() {
		return id;
	}
	public String getLogin_id() {
		return login_id;
	}
	public String getName() {
		return name;
	}
	public int getBranch_id() {
		return branch_id;
	}
	public int getPosition_id() {
		return position_id;
	}
	public int getIs_deleted() {
		return is_deleted;
	}
	public String getBranchName() {
		return branchName;
	}
	public String getPositionName() {
		return positionName;
	}
	public void setId(int id) {
		this.id = id;
	}
	public void setLogin_id(String login_id) {
		this.login_id = login_id;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setBranch_id(int branch_id) {
		this.branch_id = branch_id;
	}
	public void setPosition_id(int position_id) {
		this.position_id = position_id;
	}
	public void setIs_deleted(int is_deleted) {
		this.is_deleted = is_deleted;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public void setPositionName(String positionName) {
		this.positionName = positionName;
	}



}
