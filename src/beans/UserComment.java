package beans;

import java.io.Serializable;
import java.util.Date;

public class UserComment implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private int message_id;
    private String name;
    private String text;
    private Date created_date;
    private int user_id;
    private String user_name;
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public int getId() {
		return id;
	}
	public int getMessage_id() {
		return message_id;
	}
	public String getName() {
		return name;
	}
	public String getText() {
		return text;
	}
	public Date getCreated_date() {
		return created_date;
	}
	public int getUser_id() {
		return user_id;
	}
	public String getUser_name() {
		return user_name;
	}
	public void setId(int id) {
		this.id = id;
	}
	public void setMessage_id(int message_id) {
		this.message_id = message_id;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setText(String text) {
		this.text = text;
	}
	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}
}
