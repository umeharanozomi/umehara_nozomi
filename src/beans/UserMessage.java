package beans;

import java.io.Serializable;
import java.util.Date;

public class UserMessage implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private String title;
    private String text;
    private String category;
    private Date created_date;
    private int user_id;
    private String name;

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public int getId() {
		return id;
	}
	public String getTitle() {
		return title;
	}
	public String getText() {
		return text;
	}
	public String getCategory() {
		return category;
	}
	public Date getCreated_date() {
		return created_date;
	}
	public int getUser_id() {
		return user_id;
	}
	public String getName() {
		return name;
	}
	public void setId(int id) {
		this.id = id;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public void setText(String text) {
		this.text = text;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public void setName(String name) {
		this.name = name;
	}

}
