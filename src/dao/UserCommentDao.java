package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.UserComment;
import exception.SQLRuntimeException;

public class UserCommentDao {

    public List<UserComment> getUserComment(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("comments.id, ");
            sql.append("comments.text, ");
            sql.append("comments.message_id, ");
            sql.append("comments.created_date, ");
            sql.append("comments.user_id,");
            sql.append("users.name ");
            sql.append("FROM comments ");
            sql.append("INNER JOIN users ");
            sql.append("ON comments.user_id = users.id ");
            sql.append("ORDER BY created_date DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<UserComment> ret = toUserCommentList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserComment> toUserCommentList(ResultSet rs)
            throws SQLException {

        List<UserComment> ret = new ArrayList<UserComment>();
        try {
            while (rs.next()) {
            	int id = rs.getInt("comments.id");
                String text = rs.getString("comments.text");
                Timestamp createdDate = rs.getTimestamp("created_date");
                String userName = rs.getString("users.name");
                int messageId = rs.getInt("comments.message_id");
                int useId = rs.getInt("comments.user_id");

                UserComment comment = new UserComment();
                comment.setId(id);
                comment.setText(text);
                comment.setCreated_date(createdDate);
                comment.setUser_name(userName);
                comment.setMessage_id(messageId);
                comment.setUser_id(useId);
                ret.add(comment);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

}
