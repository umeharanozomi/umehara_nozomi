package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import exception.SQLRuntimeException;

public class RevivalManagementDao {
	public void userRevival(Connection connection, int revival) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET ");
			sql.append(" is_deleted = 0");
			sql.append(" WHERE id = ?;");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, revival);

			ps.executeUpdate();
			System.out.println(ps);
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}


}
