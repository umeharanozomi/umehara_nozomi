package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import beans.Comment;
import exception.SQLRuntimeException;

public class CommentDao {

    public void insert(Connection connection, Comment comment) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO comments ( ");
            sql.append(" message_id");
            sql.append(", text");
            sql.append(", created_date");
            sql.append(", user_id");
            sql.append(") VALUES (");
            sql.append(" ?"); // message_id
            sql.append(", ?"); // text
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(", ?");//user_id
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, comment.getMessage_id());
            ps.setString(2, comment.getText());
            ps.setInt(3, comment.getUser_id());


            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

}