/**
 *
 */
package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import exception.SQLRuntimeException;

public class UserDao {

	public void insert(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();

			sql.append("INSERT INTO users ( ");
			sql.append("id");
			sql.append(", login_id");
			sql.append(", password");
			sql.append(", name");
			sql.append(", branch_id");
			sql.append(", position_id");
			sql.append(", is_deleted");
			sql.append(") VALUES (");
			sql.append("?"); // id
			sql.append(", ?"); // login_id
			sql.append(", ?"); // password
			sql.append(", ?"); // name
			sql.append(", ?"); // branch_id
			sql.append(", ?"); // position_id
			sql.append(", 0");//is_deleted
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1, user.getId());
			ps.setString(2, user.getLoginId());
			ps.setString(3, user.getPassword());
			ps.setString(4, user.getName());
			ps.setInt(5, user.getBranchId());
			ps.setInt(6, user.getPositionId());
			//ps.setInt(7, user.getIsDeleted());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public User getUser(Connection connection, String loginId,
			String password) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE login_id = ? AND password = ? AND is_deleted = 0";

			ps = connection.prepareStatement(sql);
			ps.setString(1, loginId);
			ps.setString(2, password);



			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if (userList.isEmpty() == true) {
				return null;
			} else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<User> toUserList(ResultSet rs) throws SQLException {

		List<User> ret = new ArrayList<User>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String password = rs.getString("password");
				String name = rs.getString("name");
				int  branchId = rs.getInt("branch_id");
				int positionId = rs.getInt("position_id");
				int isDeleted = rs.getInt("is_deleted");

				User user = new User();
				user.setId(id);
				user.setLoginId(loginId);
				user.setPassword(password);
				user.setName(name);
				user.setBranchId(branchId);
				user.setPositionId(positionId);
				user.setIsDeleted(isDeleted);

				ret.add(user);
			}
			return ret;
		} finally {
			close(rs);
		}
	}


	public User getUser(Connection connection, int id) {

	    PreparedStatement ps = null;
	    try {
	        String sql = "SELECT * FROM users WHERE id = ?";

	        ps = connection.prepareStatement(sql);
	        ps.setInt(1, id);

	        ResultSet rs = ps.executeQuery();
	        List<User> userList = toUserList(rs);
	        if (userList.isEmpty() == true) {
	            return null;
	        } else if (2 <= userList.size()) {
	            throw new IllegalStateException("2 <= userList.size()");
	        } else {
	            return userList.get(0);
	        }
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
	}

    public void update(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE users SET");
            sql.append("  login_id = ?");
            if(StringUtils.isBlank(user.getPassword()) !=true){
            	sql.append(", password = ?");
            }
            sql.append(", name = ?");
            sql.append(", branch_id = ?");
            sql.append(", position_id = ?");
            sql.append(" WHERE");
            sql.append(" id = ?");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getLoginId());
            if(StringUtils.isBlank(user.getPassword()) !=true){
            	ps.setString(2, user.getPassword());
            	ps.setString(3, user.getName());
                ps.setInt(4, user.getBranchId());
                ps.setInt(5, user.getPositionId());
                ps.setInt(6, user.getId());
            }else{
            	ps.setString(2, user.getName());
                ps.setInt(3, user.getBranchId());
                ps.setInt(4, user.getPositionId());
                ps.setInt(5, user.getId());
            }

            int count = ps.executeUpdate();
            if (count == 0) {
                throw new NoRowsUpdatedRuntimeException();
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }

    }

	public boolean getLoginId(Connection connection, String loginId, int id) {

	    PreparedStatement ps = null;
	    try {
	        String sql = "SELECT * FROM users WHERE login_id = ? AND id <> ?";

	        ps = connection.prepareStatement(sql);
	        ps.setString(1, loginId);
	        ps.setInt(2, id);

	        ResultSet rs = ps.executeQuery();
	        List<User> userList = toUserList(rs);
	        if (userList.size() == 0) {
	        	return true;
	        } else {
	            return false;
	        }
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
	}


	public boolean getSignUpLoginId(Connection connection, String signUpLoginId) {

	    PreparedStatement ps = null;
	    try {
	        String sql = "SELECT * FROM users WHERE login_id = ?";

	        ps = connection.prepareStatement(sql);
	        ps.setString(1, signUpLoginId);

	        ResultSet rs = ps.executeQuery();
	        List<User> userList = toUserList(rs);
	        if (userList.size() == 0) {
	        	return true;
	        } else {
	            return false;
	        }
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
	}


}
