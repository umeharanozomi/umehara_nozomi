package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.UserManagement;
import exception.SQLRuntimeException;


public class UserManagementDao {

    public List<UserManagement> getUserManagements(Connection connection) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("users.id, ");
            sql.append("users.login_id, ");
            sql.append("users.name, ");
            sql.append("branches.name, ");
            sql.append("users.is_deleted, ");
            sql.append("positions.name ");
            sql.append("FROM  users ");
            sql.append("INNER JOIN branches ");
            sql.append("ON users.branch_id = branches.id ");
            sql.append("INNER JOIN positions ");
            sql.append("ON users.position_id = positions.id ");

            ps = connection.prepareStatement(sql.toString());


            ResultSet rs = ps.executeQuery();
            List<UserManagement> ret = toUserManagementList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserManagement> toUserManagementList(ResultSet rs)
            throws SQLException {

        List<UserManagement> ret = new ArrayList<UserManagement>();
        try {
            while (rs.next()) {
            	int id = rs.getInt("users.id");
                String loginId = rs.getString("users.login_id");
                String name = rs.getString("users.name");
                String branchName = rs.getString("branches.name");
                int is_deleted = rs.getInt("users.is_deleted");
                String positionName = rs.getString("positions.name");

                UserManagement management = new UserManagement();
                management.setId(id);
                management.setLogin_id(loginId);
                management.setName(name);
                management.setBranchName(branchName);
                management.setIs_deleted(is_deleted);
                management.setPositionName(positionName);

                ret.add(management);
            }
            return ret;
        } finally {
            close(rs);
        }
    }
}

