package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import exception.SQLRuntimeException;

public class StateManagementDao {
		public void upDate(Connection connection, int is_deleted) {

			PreparedStatement ps = null;
			try {
				StringBuilder sql = new StringBuilder();
				sql.append("UPDATE users SET ");
				sql.append(" is_deleted = 1");
				sql.append(" WHERE id = ?;");

				ps = connection.prepareStatement(sql.toString());

				ps.setInt(1, is_deleted);

				ps.executeUpdate();

			} catch (SQLException e) {
				throw new SQLRuntimeException(e);
			} finally {
				close(ps);
			}
		}



}
