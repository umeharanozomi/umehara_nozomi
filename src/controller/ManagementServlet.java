package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.User;
import beans.UserManagement;
import service.ManagementService;
import service.RevivalManagementService;
import service.StateManagementService;

@WebServlet(urlPatterns = { "/management" })
public class ManagementServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
        HttpServletResponse response) throws IOException, ServletException {


        User user = (User) request.getSession().getAttribute("loginUser");
        boolean isShowManagementForm;
        if (user != null) {
            isShowManagementForm = true;
        } else {
            isShowManagementForm = false;
        }

        List<UserManagement> managements = new ManagementService().getMessage();

        request.setAttribute("managements", managements);
        request.setAttribute("isShowManagementForm", isShowManagementForm);


        request.getRequestDispatcher("management.jsp").forward(request, response);

    }

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

				String isDeleted = request.getParameter("isDeletedflag");

				if("0".equals(isDeleted)){
					int is_deleted = Integer.parseInt(request.getParameter("is_deleted"));
					new StateManagementService().register(is_deleted);
				}


				if("1".equals(isDeleted)){
					int revival = Integer.parseInt(request.getParameter("revival"));
					new RevivalManagementService().register(revival);
				}
				response.sendRedirect("management");

	}

}
