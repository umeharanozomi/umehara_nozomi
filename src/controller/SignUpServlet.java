package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Position;
import beans.User;
import service.BranchService;
import service.PositionService;
import service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {




		List<Branch> branch = new BranchService().getBranch();
		request.setAttribute("branches", branch);
		List<Position> position = new PositionService().getPosition();
		request.setAttribute("positions", position);


		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();

		HttpSession session = request.getSession();
		User user = new User();
		//user.setId(Integer.parseInt(request.getParameter("id")));
		user.setLoginId(request.getParameter("loginId"));
		user.setPassword(request.getParameter("password"));
		user.setName(request.getParameter("name"));
		user.setBranchId(Integer.parseInt(request.getParameter("branchId")));
		user.setPositionId(Integer.parseInt(request.getParameter("positionId")));
		//user.setIsDeleted(Integer.parseInt(request.getParameter("isDeleted")));

		if (isValid(request, messages) == true) {

			new UserService().register(user);
			response.sendRedirect("./management");

		} else {

			List<Branch> branch = new BranchService().getBranch();
			request.setAttribute("branches", branch);
			List<Position> position = new PositionService().getPosition();
			request.setAttribute("positions", position);

			session.setAttribute("errorMessages", messages);
			request.setAttribute("editUser", user);
			request.getRequestDispatcher("signup.jsp").forward(request, response);
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {
		//String account = request.getParameter("account");
		String password = request.getParameter("password");
		String passwordConfirmation = request.getParameter("passwordConfirmation");
		String loginId = request.getParameter("loginId");
		String name = request.getParameter("name");
		int branch_id = Integer.parseInt(request.getParameter("branchId"));
		int position_id = Integer.parseInt(request.getParameter("positionId"));


		if (loginId.length()<6 ||loginId.length()>20 ) {
			messages.add("ログインIDを6文字以上20文字以下で入力してください");
		}
		if(!loginId.matches("^[0-9A-Za-z]+$")){
			messages.add("ログインIDを半角英数字で入力してください");
		}
		if(new UserService().getSignUpLoginId(loginId) == false){
			messages.add("ログインIDが重複しているため、登録できません");
		}
		if(!(password.equals(passwordConfirmation))){
			messages.add("パスワードが一致していません");
		}
		if(!password.matches("^[ -~]+$")){
			messages.add("パスワードを半角文字(カナを除く)で入力してください");
		}
		if(password.length()<6 || password.length()>20){
			messages.add("パスワードを6文字以上20文字以下で入力してください");
		}
		if(StringUtils.isBlank(name)){
			messages.add("名称を入力してください");
		}

		if(name.length()>10){
			messages.add("名称を10文字以下で入力してください");
		}
		if(0 ==branch_id){
			messages.add("支店名を選択してください");
		}
		if(0 == position_id){
			messages.add("部署・役職を選択してください");
		}

		// TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}