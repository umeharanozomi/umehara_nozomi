package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Position;
import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import service.BranchService;
import service.PositionService;
import service.UserService;

@WebServlet(urlPatterns = { "/settings" })
public class SettingsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();

		try {
			// ユーザーidをゲットパラメータしてint型にする
			int user_id = Integer.parseInt(request.getParameter("id"));
			// ログインユーザー情報のidを元にDBからユーザー情報取得
			User editUser = new UserService().getUser(user_id);

			if (editUser == null) {
				messages.add("不正なパラメーターです");
				session.setAttribute("errorMessages", messages);
				response.sendRedirect("management");
			}

			request.setAttribute("editUser", editUser);

			List<Branch> branch = new BranchService().getBranch();
			request.setAttribute("branches", branch);
			List<Position> position = new PositionService().getPosition();
			request.setAttribute("positions", position);

			request.getRequestDispatcher("settings.jsp").forward(request, response);

		} catch (NumberFormatException e) {
			messages.add("不正なパラメーターです、ユーザー管理画面へ戻ります");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("management");
		}

	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();
		User editUser = getEditUser(request);

		if (isValid(request, messages) == true) {

			try {
				new UserService().update(editUser);
			} catch (NoRowsUpdatedRuntimeException e) {
				messages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
				session.setAttribute("errorMessages", messages);
				request.setAttribute("editUser", editUser);
				request.getRequestDispatcher("settings.jsp").forward(request, response);
				return;
			}


			response.sendRedirect("./management");
		} else {
			session.setAttribute("errorMessages", messages);
			request.setAttribute("editUser", editUser);

			List<Branch> branch = new BranchService().getBranch();
			request.setAttribute("branches", branch);
			List<Position> position = new PositionService().getPosition();
			request.setAttribute("positions", position);

			request.getRequestDispatcher("settings.jsp").forward(request, response);
		}
	}

	private User getEditUser(HttpServletRequest request) throws IOException, ServletException {

		User editUser = new User();
		editUser.setId(Integer.parseInt(request.getParameter("id")));
		editUser.setLoginId(request.getParameter("login_id"));
		editUser.setPassword(request.getParameter("password"));
		editUser.setName(request.getParameter("name"));
		editUser.setBranchId(Integer.parseInt(request.getParameter("branchId")));
		editUser.setPositionId(Integer.parseInt(request.getParameter("positionId")));
		return editUser;
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		// String account = request.getParameter("account");
		int id = Integer.parseInt(request.getParameter("id"));
		String password = request.getParameter("password");
		String passwordConfirmation = request.getParameter("passwordConfirmation");
		String loginId = request.getParameter("login_id");
		String name = request.getParameter("name");

		if (loginId.length() < 6 || loginId.length() > 20) {
			messages.add("ログインIDを6文字以上20文字以下で入力してください");
		}
		if (!loginId.matches("^[0-9A-Za-z]+$")) {
			messages.add("ログインIDを半角英数字で入力してください");
		}
		if (new UserService().getLoginId(loginId, id) == false) {
			messages.add("ログインIDが重複しているため、登録できません");
		}
		if (name.length() > 10 || name.length() <= 1) {
			messages.add("名称を10文字以下で入力してください");
		}
		if (!(password.equals(passwordConfirmation))) {
			messages.add("パスワードが一致していません");
		}
		if (!StringUtils.isBlank(password)) {
			if (password.length() < 6 || password.length() > 20) {
				messages.add("パスワードを6文字以上20文字以下で入力してください");
			}
			if (!password.matches("^[ -~]+$")) {
				messages.add("パスワードを半角文字(カナを除く)で入力してください");
			}
		}

		// TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}
