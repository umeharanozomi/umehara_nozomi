package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Comment;
import beans.User;
import beans.UserComment;
import beans.UserMessage;
import service.CommentService;
import service.DeleteCommentService;
import service.DeleteMessageService;
import service.MessageService;


@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		String categorySeach = request.getParameter("categorySeach");
		String dateSearch = request.getParameter("dateSearch");
		String dateSearchEnd = request.getParameter("dateSearchEnd");

		if(categorySeach == null){
			categorySeach = "";
		}
		request.setAttribute("dateSearch", dateSearch);
		request.setAttribute("dateSearchEnd", dateSearchEnd);

		if(StringUtils.isEmpty(dateSearch)){
			dateSearch ="2018/01/01 0:00";

		}
		if(StringUtils.isEmpty(dateSearchEnd)){
			dateSearchEnd = "2100/01/01 0:00";

		}


		List<UserMessage> messages = new MessageService().getMessage(categorySeach,dateSearch,dateSearchEnd);
		request.setAttribute("messages", messages);
		request.setAttribute("categorySeach",categorySeach);



		List<UserComment> comments = new CommentService().getComment();
		request.setAttribute("comments", comments);


		request.getRequestDispatcher("/top.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		Comment comment = new Comment();
		String deleteflag =request.getParameter("deleteflag");
		if(deleteflag.equals("0")){
			int message_id = Integer.parseInt(request.getParameter("message_id"));
			new DeleteMessageService().register(message_id);

		}else if(deleteflag.equals("2")){

			int comment_id = Integer.parseInt(request.getParameter("comment_id"));
			new DeleteCommentService().register(comment_id);
		}else{

			HttpSession session = request.getSession();

			List<String> comments = new ArrayList<String>();

			if (isValid(request, comments) == true) {

				User user = (User) session.getAttribute("loginUser");


				comment.setText(request.getParameter("comments"));
				comment.setUser_id(user.getId());
				comment.setMessage_id(Integer.parseInt(request.getParameter("message_id")));

				new CommentService().register(comment);

			} else {

				String categorySeach = "";
				String dateSearch = "2018/01/01 0:00";
				String dateSearchEnd = Calendar.getInstance().getTime().toString();


				List<UserMessage> messages = new MessageService().getMessage(categorySeach,dateSearch,dateSearchEnd);
				request.setAttribute("messages", messages);
				List<UserComment> commentList = new CommentService().getComment();


				request.setAttribute("comments", commentList);
				request.setAttribute("errorMessages", comments);
				request.setAttribute("message_id",request.getParameter("message_id") );
				request.setAttribute("comment_text", request.getParameter("comments"));
				request.getRequestDispatcher("/top.jsp").forward(request, response);
				return;

			}
		}
		response.sendRedirect("./");
	}


	private boolean isValid(HttpServletRequest request, List<String> comments) {

		String comment = request.getParameter("comments");

		if (StringUtils.isBlank(comment) == true) {
			comments.add("コメントを入力してください");
		}
		if (500 < comment.length()) {
			comments.add("500文字以下で入力してください");
		}
		if (comments.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}
