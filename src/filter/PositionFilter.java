package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

@WebFilter(urlPatterns = { "/management", "/signup", "/settings" })
public class PositionFilter implements Filter{
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException,ServletException{
		// セッションが存在しない場合NULLを返す
		HttpSession session = ((HttpServletRequest)req).getSession(false);

		if(session == null || session.getAttribute("loginUser") == null){
				RequestDispatcher dispatcher = req.getRequestDispatcher("./");
				dispatcher.forward(req,res);
		}else{
			User user = (User) session.getAttribute("loginUser");
			if(user.getPositionId() == 1){
				chain.doFilter(req, res);
			} else {
				List<String> messages = new ArrayList<String>();
				messages.add("権限がないため、このページには入れません");
				session.setAttribute("errorMessages", messages);
				((HttpServletResponse)res).sendRedirect("./");
			}
		}
	}


	public void init(FilterConfig config) throws ServletException{}
	public void destroy(){}

}

