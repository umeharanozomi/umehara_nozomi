package filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebFilter("/*")
public class LoginFilter implements Filter{
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException,ServletException{
		// セッションが存在しない場合NULLを返す
		HttpSession session = ((HttpServletRequest)req).getSession();

		System.out.println(((HttpServletRequest) req).getServletPath());

		if(session == null || session.getAttribute("loginUser") == null){

			String path = ((HttpServletRequest) req).getServletPath();
			if(!path.equals("/login") && !path.matches(".*.css")){
				// セッションがNullならば、ログイン画面へ飛ばす
				List<String> messages =new ArrayList<String>();
				messages.add("このページを表示できません、ログインしてください");
				session.setAttribute("errorMessages", messages);
				((HttpServletResponse)res).sendRedirect("./login");

			}else{

				chain.doFilter(req, res);

			}

		}else{

			chain.doFilter(req, res);

		}
	}


	public void init(FilterConfig config) throws ServletException{}
	public void destroy(){}



}


