package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;

import dao.RevivalManagementDao;

public class RevivalManagementService {
	public void register(int revival){

        Connection connection = null;
        try {
            connection = getConnection();

            RevivalManagementDao revivalManagementDao = new RevivalManagementDao();
            revivalManagementDao.userRevival(connection, revival);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
	}
}

