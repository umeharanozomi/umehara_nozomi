package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.UserManagement;
import dao.UserManagementDao;



public class ManagementService {

    public void register(UserManagement management) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserManagementDao UserManagementDao = new UserManagementDao();
            UserManagementDao.getUserManagements(connection);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }




	public List<UserManagement> getMessage() {

		Connection connection = null;
		try {
			connection = getConnection();

			UserManagementDao UserManagementDao = new UserManagementDao();
			List<UserManagement> ret = UserManagementDao.getUserManagements(connection);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
