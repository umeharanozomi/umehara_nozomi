package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;

import dao.CommentDeleteDao;

public class DeleteCommentService {
	public void register(int comment_id){

        Connection connection = null;
        try {
            connection = getConnection();

            CommentDeleteDao commentDeleteDao = new CommentDeleteDao();
            commentDeleteDao.delete(connection, comment_id);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
	}
}
