package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;

import dao.StateManagementDao;


public class StateManagementService {
	public void register(int is_deleted){

        Connection connection = null;
        try {
            connection = getConnection();

            StateManagementDao stateManagementDao = new StateManagementDao();
            stateManagementDao.upDate(connection, is_deleted);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
	}

}
