<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>${editUser.name}の設定</title>
        <link href="css/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>

    <div class="main-contents">


    <div class="header">
		<a href="./"class="square_btn">ホーム</a>
		<a href="management"class="square_btn">戻る</a>
	</div>

	<div class="name"><h2><c:out value="${editUser.name}の設定画面" /></h2></div>

        <c:if test="${ not empty errorMessages }">
            <div class="errorMessages">
                <ul>
                    <c:forEach items="${errorMessages}" var="message">
                        <li><c:out value="${message}" />
                    </c:forEach>
                </ul>
            </div>
            <c:remove var="errorMessages" scope="session"/>
        </c:if>

        <form action="settings" method="post"><br />
            <input name="id" value="${editUser.id}" id="id" type="hidden"/>
            <label for="login_id">ログインID</label>
            <div><input name="login_id" value="${editUser.loginId}" id="login_id"/></div>
            <label for="name">名称</label>
            <div><input name="name" value="${editUser.name}" id="name"/></div>
            <label for="password">パスワード(変更用)</label>
            <div><input name="password" type="password" id="password"/></div>
            <label for="passwordConfirmation">パスワード（確認用）</label>
            <div><input name="passwordConfirmation" type="password" id="passwordConfirmation"/></div>

			<c:if test="${editUser.id != loginUser.id}">
				<label for="branch_id">支店</label>
				<div><select name="branchId">
					<c:forEach items="${branches}" var="branch">
						<option value="${branch.id}" >${branch.name}</option>
							<c:if test="${editUser.branchId == branch.id}">
							<option value="${branch.id}" selected>${branch.name}</option>
						</c:if>
					</c:forEach>
				</select></div>

			    <label for="positionId">部署</label>
				<div><select name="positionId">
					<c:forEach items="${positions}" var="position">
						<option value="${position.id}">${position.name}</option>
						<c:if test="${editUser.positionId == position.id}">
							<option value="${position.id}" selected>${position.name}</option>
						</c:if>
					</c:forEach>
				</select></div>
			</c:if>

			<c:if test="${editUser.id == loginUser.id}">
				<input type ="hidden" name = "branchId" value = "${loginUser.branchId}"/>
				<input type ="hidden" name = "positionId" value = "${loginUser.positionId}"/>
			</c:if>

            <div><input type="submit" value="登録" /></div>
        </form>
        <jsp:include page="copyright.jsp"/>
    </div>
    </body>
</html>