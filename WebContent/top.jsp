<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="js/jquery-3.3.1.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/i18n/jquery.ui.datepicker-ja.min.js"></script>
<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/ui-lightness/jquery-ui.css" >
<title>ホーム</title>
</head>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<script>
  $(function() {
    $("#datepicker").datepicker();
  });
</script>
<script>
  $(function() {
    $("#datepickerEnd").datepicker();
  });
</script>
<script type="text/javascript">
	$(function() {
		$("#slidetoggle_menu dt").on("click", function() {
			$(this).next().slideToggle();
			$(this).toggleClass("active");
		});
	});
</script>


<body>
	<div class="main-contents">
		<div class="header">
				<a href="./" class="square_btn">ホーム</a>
				<a href="newMessage" class="square_btn">新規投稿</a>
				<c:if test="${loginUser.positionId == 1}">
					<a href="management" class="square_btn">ユーザー管理</a>
				</c:if>
				<span class="logout"><a href="logout"class="logout_btn">ログアウト</a></span>
		</div>



		<br>

				<div class="name"><h2><c:out value="ホーム画面" /></h2></div>


		<c:if test="${ not empty errorComments }">
			<div class="errorComments">
				<ul>
					<c:forEach items="${errorComments}" var="comment">
						<li><c:out value="${comment}" />
					</c:forEach>
				</ul>
			</div>
	               <c:remove var="errorComments" scope="session" />
		</c:if>



		 <c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
	               <c:remove var="errorMessages" scope="session" />
		</c:if>





				<div class="search">
				<form action="./" method="get">
					<label for="categorySeach">◇カテゴリ検索</label>
					<input type="text" name="categorySeach" value="${categorySeach}">
					<br>
					<br>
					<label for="dateSearch">◇期間検索</label>
					<input type="text" name= "dateSearch" value="${dateSearch}" id="datepicker"> ～
					<input type="text" name= "dateSearchEnd" value="${dateSearchEnd}" id="datepickerEnd">
					<input type="submit"  value="検索">
				</form>
				</div>
		<br>


			<c:forEach items="${messages}" var="message">
				<div class="messages">
				<div class="message">
					<div class="account-name">
						<div class="title">
							<c:out value="${message.title}" />
						</div>
						<br>
						<div class="category">
							<c:out value="カテゴリー：${message.category}" />
						</div>
						<br>

						<div class="text">
						<c:forEach var="str" items="${fn:split(message.text,'
							')}"><c:out value="${str}" /><br></c:forEach>
						</div>
						<br>
						<span class="id">
							<c:out value="投稿者：${message.name}" />
						</span>
						<span class="date">
							<fmt:formatDate value="${message.created_date}" pattern="yyyy/MM/dd HH:mm:ss" />
						</span>

					</div>
					<br />
					<c:if test="${loginUser.id == message.user_id}">
					<form action="./" method="post">
						<input type = "hidden" name = "deleteflag" value ="0" >
						<input type = "hidden" name ="message_id" value =${message.id }>
						<div class="deleteMessage_btn">
							<input type="submit" name="delete" value="削除"onclick="return deletebtn();">
						</div>
					</form>
					</c:if>
					<br />
					<dl id="slidetoggle_menu">
					<dt class="comment_btn">コメント</dt>
					<dd class="comment_hidden">
						<div class="comments">
							<c:forEach items="${comments}" var="comment">
							<c:if test ="${message.id == comment.message_id}">
								<div class="comment">
									<div class="account-name">
										<div class="text">
										<c:forEach var="s" items="${fn:split(comment.text,'
											')}" ><c:out value="${s}" /><br></c:forEach>
										</div>
										<span class="userName">
											<c:out value="投稿者：${comment.user_name}" />
										</span>
										<span class="date">
											<fmt:formatDate value="${comment.created_date}"
												pattern="yyyy/MM/dd HH:mm:ss" />
										</span>
									</div>
								</div>
								<c:if test="${loginUser.id == comment.user_id }">
									<form action="./" method="post">
									 	<input type = "hidden" name ="deleteflag" value="2">
									 	<input type = "hidden" name = "comment_id" value = "${comment.id}">
									 	<input type = "submit" name = "deleteComment" value="削除"onclick="return deletebtn();" class="sample4">
									</form>
								</c:if>
							</c:if>
							</c:forEach>
						</div>

						<form action="./" method="post">
							<br />

							<br>
								<input type="hidden" name="message_id" value="${message.id}" />

								<input type = "hidden" name = "deleteflag" value ="1" >
								<textarea name="comments" cols="60" rows="2" class="tweet-box" placeholder="コメントする.."><c:if test="${message.id == message_id}"><c:out value="${comment_text}"></c:out></c:if></textarea>
					 			<br />
					 			<!-- <div class="comenntpost_btn"> -->
					 				<input type="submit" value="投稿" class="comenntpost_btn">
					 			<!-- </div> -->
					 			<br />
				 		</form>
					</dd>
					</dl>
				 		<br />
				</div>
				</div>
			</c:forEach>
		</div>


	<jsp:include page="copyright.jsp"/>
<script type="text/javascript">
function deletebtn() {
	var result = window.confirm('削除しても宜しいですか？');

	if (result) {

		return true;

	}
	return false;
}
</script>
</body>
</html>
