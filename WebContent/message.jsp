<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="css/style.css" rel="stylesheet" type="text/css">
<title>新規投稿</title>
</head>
<body>
	<div class="form-area">
			<a href="./"class="square_btn">ホーム</a>

			<div class="name"><h2><c:out value="新規投稿画面" /></h2></div>

			<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
	               <c:remove var="errorMessages" scope="session" />
		</c:if>

		<form action="newMessage" method="post">
			<br>
			<label for="title">◆件名<br>(30文字以下で入力してください)</label>
			<div><input name="title" value="${message.title}" class = "messageTitle"/></div>
			<br />
			<br>
			◆本文<br>(1000文字以下で入力してください)
			<br />
			<textarea name="text" cols="100" rows="5"><c:out value ="${message.text}"></c:out></textarea>
			<br />
			<br>
			<label for="category">◆カテゴリー<br>(10文字以下で入力してください)</label>
			<div><input name="category"value="${message.category}" class= "messageCategory"/></div>
			<br />
			<br />
			<input type="submit" value="投稿">
		</form>
		<c:remove var="message" scope="session" />

		<jsp:include page="copyright.jsp"/>
	</div>
</body>
</html>
