<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザ管理</title>
</head>
<body>
	<div class="main-contents">
		<div class="header">
				<a href="./"class="square_btn">ホーム</a>
				<a href="signup"class="square_btn">ユーザー新規登録</a>
				<span class="logout"><a href="logout"class="logout_btn">ログアウト</a></span>
		</div>





	<div class="name"><h2><c:out value="ユーザー管理画面" /></h2></div>

    <c:if test="${ not empty errorMessages }">
       <div class="errorMessages">
           <ul>
               <c:forEach items="${errorMessages}" var="message">
                   <li><c:out value="${message}" />
               </c:forEach>
           </ul>
       </div>
       <c:remove var="errorMessages" scope="session"/>
   	</c:if>


	<table class="managements">
	 <tr><th>ログインID</th><th>名称</th><th>支店</th><th>部署・役職</th><th>ユーザーの状態</th><th>編集</th></tr>
		<c:forEach items="${managements}" var="management">
			<tr><td><c:out value="${management.login_id}" />
			</td><td><c:out value="${management.name}" />
			</td><td><c:out value="${management.branchName}" />
			</td><td><c:out value="${management.positionName}" />
				<c:if test="${loginUser.id == management.id}">
					</td><td><div class= "loginText"><c:out value="ログイン中"></c:out></div>
				</c:if>


				<c:if test="${loginUser.id != management.id}">
					<c:if test="${management.is_deleted == 0}">
						<form action="management" method="post">
							<input type = "hidden" name = "isDeletedflag" value = "0">
							<input type = "hidden" name ="is_deleted" value ="${management.id}">
							</td><td><div class = "is_deletedBtn"><span class= "is_deletedText"><c:out value="使用可"></c:out></span><input type="submit" name=is_deleted value="停止" onclick="return submitbtn();"></div>
						</form>
					</c:if>
					<c:if test="${management.is_deleted == 1}">
						<form action="management" method="post">
							<input type = "hidden" name = "isDeletedflag" value = "1">
							<input type = "hidden" name = "revival" value = "${management.id}">
							</td><td><div class="revival"><span class= "revivalText"><c:out value="停止中"></c:out></span><input type = "submit" name = "revival" value = "復活" onclick="return submitbtn();"></div>
						</form>
					</c:if>
					</c:if>
				</td><td><div class= "settingBotan"><a href="settings?id=${management.id}"class="setting_btn">編集</a></div></td></tr>
		</c:forEach>
	</table>
	<jsp:include page="copyright.jsp"/>
	</div>
<script type="text/javascript">
function submitbtn() {
	var result = window.confirm('変更しても宜しいですか？');

	if (result) {

		return true;

	}
	return false;
}
</script>
</body>
</html>