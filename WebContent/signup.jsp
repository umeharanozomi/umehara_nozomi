<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<title>ユーザー新規登録</title>
</head>
<body>
	<div class="signup">
		<div class="header">
			<a href="./"class="square_btn">ホーム</a>
			<a href="management"class="square_btn">戻る</a>
		</div>

		<div class="name"><h2><c:out value="ユーザー新規登録画面" /></h2></div>


		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
		<form action="signup" method="post">
			<br />
			<div><label for="loginId">ログインID</label><span class= "hissu"><c:out value="必須*"></c:out></span></div>
			<input name="loginId" value="${editUser.loginId}" />
			<br />
			<div><label for="name">名称</label><span class= "hissu"><c:out value="必須*"></c:out></span></div>
			<input name="name" value="${editUser.name}" />
			<br />
			<div><label for="password">パスワード</label><span class= "hissu"><c:out value="必須*"></c:out></span></div>
			<input name="password" type="password" id="password" />
			<br />
			<label for="passwordConfirmation">パスワード(確認用)</label>
			<br />
			<input name="passwordConfirmation" type="password" id="passwordConfirmation" />
			<br />
			<div><label for="branchId">支店</label><span class= "hissu"><c:out value="必須*"></c:out></span></div>
			<select name="branchId">
				<option value="0">選択してください</option>
				<c:forEach items="${branches}" var="branch">
					<c:if test="${editUser.branchId == branch.id}">
						<option value="${branch.id}" selected>${branch.name}</option>
					</c:if>
					<c:if test="${editUser.branchId != branch.id}">
						<option value="${branch.id}" >${branch.name}</option>
					</c:if>
				</c:forEach>
			</select>
			<br />
			<div><label for="positionId">部署・役職</label><span class= "hissu"><c:out value="必須*"></c:out></span></div>

			<select name="positionId">
				<option value="0">選択してください</option>
				<c:forEach items="${positions}" var="position">
					<c:if test="${editUser.positionId != position.id}">
						<option value="${position.id}">${position.name}</option>
					</c:if>
					<c:if test="${editUser.positionId == position.id}">
						<option value="${position.id}" selected>${position.name}</option>
					</c:if>
				</c:forEach>
			</select>
			<br />
			<br />
			<input type="submit" value="登録" />
			<br />
		</form>
		<jsp:include page="copyright.jsp"/>
	</div>

</body>
</html>